/*
 * Copyright (c) 1998-2015 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2015 Stony Brook University
 * Copyright (c) 2003-2015 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "trfs.h"

int acquire_lock_write(struct super_block *sb, struct trfs_record *rec)
{
	int err = 0;
	struct list_records *record = NULL;

	record = kzalloc(sizeof(struct list_records), GFP_KERNEL);
	if (!record) {
		err = -ENOMEM;
		goto out;
	}
	record->rec = rec;
	INIT_LIST_HEAD(&record->list_node);

	/* acquire lock and increment the id*/
	spin_lock(&TRFS_SB(sb)->list_lock);

	TRFS_SB(sb)->recid = TRFS_SB(sb)->recid + 1;
	rec->id = TRFS_SB(sb)->recid;
	list_add_tail(&(record->list_node), TRFS_SB(sb)->head_node);

	spin_unlock(&TRFS_SB(sb)->list_lock);

out:
	return err;
}

/* allocates ex_len bytes for structure and initialize common fields */
void *init_record(struct file *file, struct dentry *dentry, int ex_len)
{
	void *buf_rec;
	void *path_buf = NULL;
	char *path;
	int len_path, totlen;
	struct trfs_record *rec = NULL;


	path_buf = kmalloc(512, GFP_KERNEL);
	if (path_buf == NULL) {
		buf_rec = NULL;
		goto out;
	}
	memset(path_buf, '\0', 512);

	path = (char *)path_buf;
	path = dentry_path_raw(dentry, path, 512);
	if (IS_ERR(path)) {
		buf_rec = NULL;
		goto out;
	}

	len_path = strlen(path);
	totlen = len_path + sizeof(struct trfs_record) + ex_len;

	/*create the structure*/
	buf_rec = kmalloc(totlen, GFP_KERNEL);
	if (buf_rec == NULL)
		goto out;
	rec = (struct trfs_record *)buf_rec;
	rec->size_of_record = totlen;
	rec->fileptr = (long)file;

	if (file != NULL) {
		rec->flags = file->f_flags;
		rec->mode = dentry->d_inode->i_mode;
	}
	rec->len_of_path = len_path;
	strcpy(&rec->pathname[0], path);

out:
	if (path_buf != NULL)
		kfree(path_buf);
	return buf_rec;

}

static ssize_t trfs_read(struct file *file, char __user *buf,
			   size_t count, loff_t *ppos)
{
	int err, res, exlen;
	struct file *lower_file;
	struct dentry *dentry = file->f_path.dentry;
	struct trfs_record *rec;
	struct super_block *sb;
	struct read_write *ptr;
	void *buf_rec = NULL, *read_buf = NULL;
	int fpos = *ppos;

	memset(buf, '\0', count);
	lower_file = trfs_lower_file(file);
	err = vfs_read(lower_file, buf, count, ppos);
	exlen = sizeof(struct read_write);

	/* update our inode atime upon a successful lower read */
	if (err >= 0) {
		fsstack_copy_attr_atime(d_inode(dentry),
					file_inode(lower_file));

		exlen = exlen + err;
	}

	sb = d_inode(dentry)->i_sb;
	if (!(TRFS_SB(sb)->bitmap & TRFS_IOC_READ))
		goto out;
	buf_rec = init_record(file, dentry, exlen);
	if (buf_rec == NULL)
		goto out;
	rec = (struct trfs_record *)buf_rec;
	rec->type = 'r';
	rec->err = err;
	read_buf = kmalloc(exlen, GFP_KERNEL);
	if (read_buf == NULL)
		goto out;
	ptr = (struct read_write *)read_buf;
	ptr->ppos = fpos;
	ptr->count = count;
	strcpy(&ptr->data_buf[0], buf);

	/*copy the read_write struct into parent struct*/
	memcpy(TRFS_EXTRA(buf_rec, rec), ptr, exlen);
	res = acquire_lock_write(sb, rec);

	if (res < 0)
		printk(KERN_ERR "Error in recording the READ operation\n");
out:
		if (read_buf != NULL)
			kfree(read_buf);
	return err;
}

static ssize_t trfs_write(struct file *file, const char __user *buf,
			    size_t count, loff_t *ppos)
{
	int err, res, exlen;

	struct file *lower_file;
	struct dentry *dentry = file->f_path.dentry;
	struct trfs_record *rec;
	struct super_block *sb;
	struct read_write *ptr;

	void *buf_rec = NULL, *read_buf = NULL;
	int fpos = *ppos;
	lower_file = trfs_lower_file(file);
	err = vfs_write(lower_file, buf, count, ppos);
	exlen = sizeof(struct read_write);
	/* update our inode times+sizes upon a successful lower write */
	if (err >= 0) {
		fsstack_copy_inode_size(d_inode(dentry),
					file_inode(lower_file));
		fsstack_copy_attr_times(d_inode(dentry),
					file_inode(lower_file));
		exlen = exlen + err;
	}
	sb = d_inode(dentry)->i_sb;
	if (!(TRFS_SB(sb)->bitmap & TRFS_IOC_WRITE))
		goto out;
	buf_rec = init_record(file, dentry, exlen);
	if (buf_rec == NULL)
		goto out;
	rec = (struct trfs_record *)buf_rec;
	rec->type = 'w';
	rec->err = err;
	read_buf = kmalloc(exlen, GFP_KERNEL);
	if (read_buf == NULL)
		goto out;
	ptr = (struct read_write *)read_buf;
	ptr->ppos = fpos;
	ptr->count = count;

	strcpy(&ptr->data_buf[0], buf);

	/*copy the read_write struct into parent struct*/
	memcpy(TRFS_EXTRA(buf_rec, rec), ptr, exlen);
	res = acquire_lock_write(sb, rec);

	if (res < 0)
		printk(KERN_ERR "Error in recording the WRITE operation\n");
out:
		if (read_buf != NULL)
			kfree(read_buf);

	return err;
}

static int trfs_readdir(struct file *file, struct dir_context *ctx)
{
	int err;
	struct file *lower_file = NULL;
	struct dentry *dentry = file->f_path.dentry;

	lower_file = trfs_lower_file(file);
	err = iterate_dir(lower_file, ctx);
	file->f_pos = lower_file->f_pos;
	if (err >= 0)		/* copy the atime */
		fsstack_copy_attr_atime(d_inode(dentry),
					file_inode(lower_file));
	return err;
}

static long trfs_unlocked_ioctl(struct file *file, unsigned int cmd,
				  unsigned long arg)
{
	long err = 0;
	struct file *lower_file;
	uint32_t *bitmap;
	int notcopied = 0;
	struct super_block *sb = file->f_path.dentry->d_inode->i_sb;

	#ifdef EC
	int op = 0x0;
	uint32_t bit_status = 0;
	#endif
	bitmap = &(TRFS_SB(sb)->bitmap);

	switch (cmd) {
	case TRFS_IOCTL_GETBITMAP:
		notcopied = copy_to_user((uint32_t *)arg, bitmap, sizeof(uint32_t));
		if (notcopied) {
			err = -EAGAIN;
			goto out;
		}
		break;
	case TRFS_IOCTL_SETBITMAP:
		notcopied = copy_from_user(bitmap, (uint32_t *)arg, sizeof(uint32_t));
		if (notcopied) {
			err = -EAGAIN;
			goto out;
		}
		break;
	}

	#ifdef EC
	/* Get set/reset status of an operation, in case of incremental ioctl */
	if (cmd != TRFS_IOCTL_GETBITMAP && cmd != TRFS_IOCTL_SETBITMAP) {
		notcopied = copy_from_user(&bit_status, (uint32_t *)arg, sizeof(uint32_t));
		if (notcopied) {
			err = -EAGAIN;
			goto out;
		}
		switch (cmd) {
		case TRFS_IOCTL_SETREAD:
			op = TRFS_IOC_READ;
			break;
		case TRFS_IOCTL_SETWRITE:
			op = TRFS_IOC_WRITE;
			break;
		case TRFS_IOCTL_SETOPEN:
			op = TRFS_IOC_OPEN;
			break;
		case TRFS_IOCTL_SETLINK:
			op = TRFS_IOC_LINK;
			break;
		case TRFS_IOCTL_SETUNLINK:
			op = TRFS_IOC_UNLINK;
			break;
		case TRFS_IOCTL_SETMKDIR:
			op = TRFS_IOC_MKDIR;
			break;
		case TRFS_IOCTL_SETRMDIR:
			op = TRFS_IOC_RMDIR;
			break;
		case TRFS_IOCTL_SETSYMLINK:
			op = TRFS_IOC_SYMLINK;
			break;
		case TRFS_IOCTL_SETCLOSE:
			op = TRFS_IOC_CLOSE;
			break;
		case TRFS_IOCTL_SETRENAME:
			op = TRFS_IOC_RENAME;
			break;
		}
		if (bit_status)
			*bitmap = *bitmap | op;
		else
			*bitmap = *bitmap & ~op;
	}
	#endif
	goto out;

	err = -ENOTTY;
	lower_file = trfs_lower_file(file);

	/* XXX: use vfs_ioctl if/when VFS exports it */
	if (!lower_file || !lower_file->f_op)
		goto out;
	if (lower_file->f_op->unlocked_ioctl)
		err = lower_file->f_op->unlocked_ioctl(lower_file, cmd, arg);

	/* some ioctls can change inode attributes (EXT2_IOC_SETFLAGS) */
	if (!err)
		fsstack_copy_attr_all(file_inode(file),
			file_inode(lower_file));


out:
	return err;
}

#ifdef CONFIG_COMPAT
static long trfs_compat_ioctl(struct file *file, unsigned int cmd,
				unsigned long arg)
{
	long err = -ENOTTY;
	struct file *lower_file;

	lower_file = trfs_lower_file(file);

	/* XXX: use vfs_ioctl if/when VFS exports it */
	if (!lower_file || !lower_file->f_op)
		goto out;
	if (lower_file->f_op->compat_ioctl)
		err = lower_file->f_op->compat_ioctl(lower_file, cmd, arg);

out:
	return err;
}
#endif

static int trfs_mmap(struct file *file, struct vm_area_struct *vma)
{
	int err = 0;
	bool willwrite;
	struct file *lower_file;
	const struct vm_operations_struct *saved_vm_ops = NULL;

	/* this might be deferred to mmap's writepage */
	willwrite = ((vma->vm_flags | VM_SHARED | VM_WRITE) == vma->vm_flags);

	/*
	 * File systems which do not implement ->writepage may use
	 * generic_file_readonly_mmap as their ->mmap op.  If you call
	 * generic_file_readonly_mmap with VM_WRITE, you'd get an -EINVAL.
	 * But we cannot call the lower ->mmap op, so we can't tell that
	 * writeable mappings won't work.  Therefore, our only choice is to
	 * check if the lower file system supports the ->writepage, and if
	 * not, return EINVAL (the same error that
	 * generic_file_readonly_mmap returns in that case).
	 */
	lower_file = trfs_lower_file(file);
	if (willwrite && !lower_file->f_mapping->a_ops->writepage) {
		err = -EINVAL;
		printk(KERN_ERR "trfs: lower file system does not "
		       "support writeable mmap\n");
		goto out;
	}

	/*
	 * find and save lower vm_ops.
	 *
	 * XXX: the VFS should have a cleaner way of finding the lower vm_ops
	 */
	if (!TRFS_F(file)->lower_vm_ops) {
		err = lower_file->f_op->mmap(lower_file, vma);
		if (err) {
			printk(KERN_ERR "trfs: lower mmap failed %d\n", err);
			goto out;
		}
		saved_vm_ops = vma->vm_ops; /* save: came from lower ->mmap */
	}

	/*
	 * Next 3 lines are all I need from generic_file_mmap.  I definitely
	 * don't want its test for ->readpage which returns -ENOEXEC.
	 */
	file_accessed(file);
	vma->vm_ops = &trfs_vm_ops;

	file->f_mapping->a_ops = &trfs_aops; /* set our aops */
	if (!TRFS_F(file)->lower_vm_ops) /* save for our ->fault */
		TRFS_F(file)->lower_vm_ops = saved_vm_ops;

out:
	return err;
}

static int trfs_open(struct inode *inode, struct file *file)
{
	int err = 0, res = 0;
	struct file *lower_file = NULL;
	struct path lower_path;
	struct trfs_record *rec = NULL;
	void *buf_rec = NULL;

	/* don't open unhashed/deleted files */
	if (d_unhashed(file->f_path.dentry)) {
		err = -ENOENT;
		goto trace_op;
	}

	file->private_data =
		kzalloc(sizeof(struct trfs_file_info), GFP_KERNEL);
	if (!TRFS_F(file)) {
		err = -ENOMEM;
		goto trace_op;
	}

	/* open lower object and link trfs's file struct to lower's */
	trfs_get_lower_path(file->f_path.dentry, &lower_path);
	lower_file = dentry_open(&lower_path, file->f_flags, current_cred());
	path_put(&lower_path);
	if (IS_ERR(lower_file)) {
		err = PTR_ERR(lower_file);
		lower_file = trfs_lower_file(file);
		if (lower_file) {
			trfs_set_lower_file(file, NULL);
			fput(lower_file); /* fput calls dput for lower_dentry */
		}
	} else
		trfs_set_lower_file(file, lower_file);
	if (err)
		kfree(TRFS_F(file));
	else
		fsstack_copy_attr_all(inode, trfs_lower_inode(inode));

trace_op:
	/* check if bitmap is set for this operation*/
	if (!(TRFS_SB(inode->i_sb)->bitmap & TRFS_IOC_OPEN))
		goto out_err;
	/* initialize record and add it to list */
	buf_rec = init_record(file, file->f_path.dentry, 0);
	if (buf_rec == NULL)
		goto out_err;
	rec = (struct trfs_record *)buf_rec;
	rec->type = 'o';
	rec->err = err;
	res = acquire_lock_write(file->f_path.dentry->d_inode->i_sb, rec);

	if (res < 0)
		printk(KERN_ERR "Error in recording OPEN operation\n");

out_err:

	return err;
}

static int trfs_flush(struct file *file, fl_owner_t id)
{
	int err = 0;
	struct file *lower_file = NULL;

	lower_file = trfs_lower_file(file);
	if (lower_file && lower_file->f_op && lower_file->f_op->flush) {
		filemap_write_and_wait(file->f_mapping);
		err = lower_file->f_op->flush(lower_file, id);
	}

	return err;
}

/* release all lower object references & free the file info structure */
static int trfs_file_release(struct inode *inode, struct file *file)
{
	struct file *lower_file;
	struct trfs_record *rec = NULL;
	void *buf_rec = NULL;
	int res;

	lower_file = trfs_lower_file(file);
	if (lower_file) {
		trfs_set_lower_file(file, NULL);
		fput(lower_file);
	}

	if (!(TRFS_SB(inode->i_sb)->bitmap & TRFS_IOC_CLOSE))
		goto out;

	buf_rec = init_record(file, file->f_path.dentry, 0);
	if (buf_rec == NULL) {
		printk("NULL reached : rec %d \n", (buf_rec == NULL));
		goto out;
	}
	rec = (struct trfs_record *)buf_rec;
	rec->type = 'c';
	rec->err = 0;
	if (!(TRFS_SB(inode->i_sb)->bitmap & TRFS_IOC_CLOSE))
		goto out;

	buf_rec = init_record(file, file->f_path.dentry, 0);
	if (buf_rec == NULL)
		goto out;
	rec = (struct trfs_record *)buf_rec;
	rec->type = 'c';
	rec->err = 0;
	res = acquire_lock_write(inode->i_sb, rec);
	if (res < 0)
		printk(KERN_ERR "Error in recording CLOSE operation\n");
out:
	kfree(TRFS_F(file));
	return 0;
}

static int trfs_fsync(struct file *file, loff_t start, loff_t end,
			int datasync)
{
	int err;
	struct file *lower_file;
	struct path lower_path;
	struct dentry *dentry = file->f_path.dentry;

	err = __generic_file_fsync(file, start, end, datasync);
	if (err)
		goto out;
	lower_file = trfs_lower_file(file);
	trfs_get_lower_path(dentry, &lower_path);
	err = vfs_fsync_range(lower_file, start, end, datasync);
	trfs_put_lower_path(dentry, &lower_path);
out:
	return err;
}

static int trfs_fasync(int fd, struct file *file, int flag)
{
	int err = 0;
	struct file *lower_file = NULL;

	lower_file = trfs_lower_file(file);
	if (lower_file->f_op && lower_file->f_op->fasync)
		err = lower_file->f_op->fasync(fd, lower_file, flag);

	return err;
}

/*
 * Wrapfs cannot use generic_file_llseek as ->llseek, because it would
 * only set the offset of the upper file.  So we have to implement our
 * own method to set both the upper and lower file offsets
 * consistently.
 */
static loff_t trfs_file_llseek(struct file *file, loff_t offset, int whence)
{
	int err;
	struct file *lower_file;

	err = generic_file_llseek(file, offset, whence);
	if (err < 0)
		goto out;

	lower_file = trfs_lower_file(file);
	err = generic_file_llseek(lower_file, offset, whence);

out:
	return err;
}

/*
 * Wrapfs read_iter, redirect modified iocb to lower read_iter
 */
ssize_t
trfs_read_iter(struct kiocb *iocb, struct iov_iter *iter)
{
	int err;
	struct file *file = iocb->ki_filp, *lower_file;

	lower_file = trfs_lower_file(file);
	if (!lower_file->f_op->read_iter) {
		err = -EINVAL;
		goto out;
	}

	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->read_iter(iocb, iter);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode atime as needed */
	if (err >= 0 || err == -EIOCBQUEUED)
		fsstack_copy_attr_atime(d_inode(file->f_path.dentry),
					file_inode(lower_file));
out:
	return err;
}

/*
 * Wrapfs write_iter, redirect modified iocb to lower write_iter
 */
ssize_t
trfs_write_iter(struct kiocb *iocb, struct iov_iter *iter)
{
	int err;
	struct file *file = iocb->ki_filp, *lower_file;

	lower_file = trfs_lower_file(file);
	if (!lower_file->f_op->write_iter) {
		err = -EINVAL;
		goto out;
	}

	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->write_iter(iocb, iter);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode times/sizes as needed */
	if (err >= 0 || err == -EIOCBQUEUED) {
		fsstack_copy_inode_size(d_inode(file->f_path.dentry),
					file_inode(lower_file));
		fsstack_copy_attr_times(d_inode(file->f_path.dentry),
					file_inode(lower_file));
	}
out:
	return err;
}

const struct file_operations trfs_main_fops = {
	.llseek		= generic_file_llseek,
	.read		= trfs_read,
	.write		= trfs_write,
	.unlocked_ioctl	= trfs_unlocked_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl	= trfs_compat_ioctl,
#endif
	.mmap		= trfs_mmap,
	.open		= trfs_open,
	.flush		= trfs_flush,
	.release	= trfs_file_release,
	.fsync		= trfs_fsync,
	.fasync		= trfs_fasync,
	.read_iter	= trfs_read_iter,
	.write_iter	= trfs_write_iter,
};

/* trimmed directory options */
const struct file_operations trfs_dir_fops = {
	.llseek		= trfs_file_llseek,
	.read		= generic_read_dir,
	.iterate	= trfs_readdir,
	.unlocked_ioctl	= trfs_unlocked_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl	= trfs_compat_ioctl,
#endif
	.open		= trfs_open,
	.release	= trfs_file_release,
	.flush		= trfs_flush,
	.fsync		= trfs_fsync,
	.fasync		= trfs_fasync,
};
