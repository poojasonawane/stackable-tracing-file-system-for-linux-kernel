#include <linux/ioctl.h>
#define TRFS_IOCTL_MAGIC 'M'

/*uncomment this for checking EC*/
/*#define EC 1*/

#define TRFS_IOCTL_GETBITMAP _IOR(TRFS_IOCTL_MAGIC, 0, uint32_t)
#define TRFS_IOCTL_SETBITMAP _IOW(TRFS_IOCTL_MAGIC, 1, uint32_t)

#define TRFS_IOC_READ 0x01
#define TRFS_IOC_WRITE 0x02
#define TRFS_IOC_OPEN 0x04
#define TRFS_IOC_CLOSE 0x08
#define TRFS_IOC_LINK 0x10
#define TRFS_IOC_UNLINK 0x20
#define TRFS_IOC_MKDIR 0x40
#define TRFS_IOC_RMDIR 0x80
#define TRFS_IOC_SYMLINK 0x100
#define TRFS_IOC_RENAME 0x200

#define TRFS_IOC_ALL 0x3ff

#ifdef EC
#define TRFS_IOCTL_SETREAD _IOW(TRFS_IOCTL_MAGIC, 2, uint32_t)
#define TRFS_IOCTL_SETWRITE _IOW(TRFS_IOCTL_MAGIC, 3, uint32_t)
#define TRFS_IOCTL_SETOPEN _IOW(TRFS_IOCTL_MAGIC, 4, uint32_t)
#define TRFS_IOCTL_SETCLOSE _IOW(TRFS_IOCTL_MAGIC, 5, uint32_t)
#define TRFS_IOCTL_SETLINK _IOW(TRFS_IOCTL_MAGIC, 6, uint32_t)
#define TRFS_IOCTL_SETUNLINK _IOW(TRFS_IOCTL_MAGIC, 7, uint32_t)
#define TRFS_IOCTL_SETSYMLINK _IOW(TRFS_IOCTL_MAGIC, 8, uint32_t)
#define TRFS_IOCTL_SETMKDIR _IOW(TRFS_IOCTL_MAGIC, 9, uint32_t)
#define TRFS_IOCTL_SETRMDIR _IOW(TRFS_IOCTL_MAGIC, 10, uint32_t)
#define TRFS_IOCTL_SETRENAME _IOW(TRFS_IOCTL_MAGIC, 11, uint32_t)

#endif

/* read/write info */
struct read_write {
	int ppos;
	size_t count;
	char data_buf[1];
};

/* record for each operation */
struct trfs_record {
	unsigned int id;
	short size_of_record;
	char type;
	unsigned int flags;
	unsigned int mode;
	int err;
	long fileptr;
	short len_of_path;
	char pathname[1];
};

/* macro to access memory allocated for operation specific information */
#define TRFS_EXTRA(buf, rec) ((void *)(buf+sizeof(struct trfs_record)+rec->len_of_path))
