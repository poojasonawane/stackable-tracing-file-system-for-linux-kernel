README
------

ASSIGNMENT OVERVIEW
-------------------

In this assignment, we modified the WRAPFS stackable file system to add tracing support. This program will generate a trace file which can be replayed in userland. We have intercepted following operations in our current implementation:
	a. Open a file
	b. Close file
	c. Read
	d. Write
	e. File rename
	f. Make directory
	g. Remove directory
	h. Link
	i. Symlink
	j. Unlink

Treplay and trctl are the user-level C programs that are used to replay trace file in userland and controlling the f/s methods that are intercepted and traced, respectively.

COMPILATION AND TESTING
-----------------------

The user code is placed in hw2/ and the TRFS code in fs/trfs directories. Makefile is added for compilation/building modules in these directories.
	mount -t trfs -o tfile=$(trace_file_path) $(mount_device_path) $(mount_point) will mount the given_mount_device path to the mount_point. The trace file will be generated at trace_file_path.

Usage of user-level programs:
	./treplay [-ns] TFILE
The above command will replay the trace file given by TFILE
-n: show what records will be replayed without replaying
-s: strict mode -- abort replaying as soon as a deviation occurs

This program must specify the path of the directory where the tfile should be replayed. 
This needs to be set in hw2/trctl.c as a string to the variable 'basepath' in get_filepath function.

	./trctl CMD /mounted/path
The above command will issue an ioctl(2) to trfs to set the bitmap of which f/s methods to trace
CMD can be: 
	"all": to enable tracing for all f/s methods
	"none": to disable tracing for all f/s methods
	0xNN: hex number to pass bitmap of methods to trace or not

EXTRA CREDIT
------------
We added incremental ioctl support, which can be enabled by adding following command in fs/trfs/trfsdefs.h
	#define EC 1

In addition to CMD in trctl, this allows user to set/reset each individual operation such as +read -write, etc. Operations are parsed from left to right. In order to implement this, we have extended the ioctl api to support these additional requests. Following are the request codes which can be used in these calls:
	TRFS_IOCTL_GETBITMAP - get the current bitmap
	TRFS_IOCTL_SETBITMAP - set current bitmap with the given argument
For each of the operations, TRFS_IOCTL_SETXXXXXX can be used (eg. TRFS_IOCTL_SETREAD, TRFS_IOCTL_SETWRITE, etc)

DESIGN
------

Following are the incremental modifications which we did in the data structure of our trace record over the time of implementation.
- Initially, we had a single structure with common fields and a data buffer for operation specific information. But we realized that we would be wasting a lot of space as not all records will have all the fields.
- Then we thought of having a separate structure for storing each operation. But then this would have increased the overhead of adding any new operation support. Also, we found that there were some fields which were common for all operations (such as permissions, errno, etc). So, in order to avoid storing these common fields in different structure, we came up with the following design.
- Our implementation captures the required information for each operation in a nested structure. The parent structure is common to all the operations and includes the following fields: 
	- Record ID
	- Record size
	- Record type
	- Flags
	- Permission mode
	- Error
	- File pointer
	- Length of path
	- Path
We are storing operation specific information in separate structure which is allocated below the parent structure. For and read and write operations, this structure includes:
	- File pointer position
	- #bytes read/written
	- Data
Operations like open, close, mkdir, rmdir, unlink donot need any extra information apart from the parent structure, whereas for rename and symlink, we are storing the new file/dir name below the parent structure. This way,we achieve object composition and modularity and code reuse.

Rather than writing records directly to tfile, each of these record structures are maintained in a linked list and a spinlock is used to ensure protected access while adding/deleting nodes to this list. This data structure is assisted by a kthread for writing the records to tfile. 
In linked list, nodes are added by the intercepted operations and are deleted by kthread. Records are written in binary format in tfile, which ensures they are compact. In the end, when the f/s is unmounted, the module waits for the kthread empty this list d/s so as to ensure that all the records are written correctly before unmounting.

In userland, we are maintaing a linked list to store mapping of already opened files and their respective file descriptors. This ensures that the operations performed using any particular file descriptor are properly mapped to it, while replaying. For this, we referred the linked list implementation from https://www.tutorialspoint.com/data_structures_algorithms/linked_list_program_in_c.htm
