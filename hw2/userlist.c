#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

struct node {
	int data;
	long key;
   	struct node *next;
};

struct node *head = NULL;
struct node *current = NULL;

//is list empty
bool isEmpty() {
 	return head == NULL;
}

//find a link with given key
int find(long key) {

   	struct node* current = head;

   	if(head == NULL)
      		return -1;

   	while(current->key != key) {
	
      		//if it is last node
      		if(current->next == NULL) {
        		 return -1;
      		} else {
         	//go to next link
         		current = current->next;
      		}
   	}      
	
   //if data found, return the current Link
   if(current == NULL)
	return -1;
   return current->data;
}

//delete a link with given key
struct node* delete(long key) {
   	struct node* current = head;
   	struct node* previous = NULL;
	
   	if(head == NULL)
      		return NULL;

	while(current->key != key) {

      	if(current->next == NULL) {
         	return NULL;
      	} else {
        	previous = current;
         	current = current->next;
      	}
   }

   if(current == head)
   	head = head->next;
   else 
	previous->next = current->next;
	
	return current;
}

//insert link at the first location
void insertNode(long key, int data) {
	struct node *new_node = (struct node*) malloc(sizeof(struct node));
	new_node->key = key;
 	new_node->data = data;
	new_node->next = head;	
	head = new_node;
}
