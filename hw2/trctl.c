#include <stdio.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include "../fs/trfs/trfsdefs.h"

#ifdef EC
int get_trfs_op(char *cmd){
	int op = 0;

	if (!strcmp(cmd, "ll") || !strcmp(cmd, "one") || !strncmp(cmd,"x",1)){
		op = TRFS_IOCTL_SETBITMAP;
	}
	else if (!strcmp(cmd, "read")){
		op = TRFS_IOCTL_SETREAD;
	}
	else if (!strcmp(cmd, "write")){
                op = TRFS_IOCTL_SETWRITE;
        }
	else if (!strcmp(cmd, "open")){
                op = TRFS_IOCTL_SETOPEN;
        }
	else if (!strcmp(cmd, "link")){
                op = TRFS_IOCTL_SETLINK;
        }
	else if (!strcmp(cmd, "unlink")){
                op = TRFS_IOCTL_SETUNLINK;
        }
	else if (!strcmp(cmd, "mkdir")){
                op = TRFS_IOCTL_SETMKDIR;
        }
	else if (!strcmp(cmd, "rmdir")){
                op = TRFS_IOCTL_SETRMDIR;
        }
	else if (!strcmp(cmd, "symlink")){
                op = TRFS_IOCTL_SETSYMLINK;
        }
	else if (!strcmp(cmd, "close")){
                op = TRFS_IOCTL_SETCLOSE;
        }
	else if (!strcmp(cmd, "rename")){
                op = TRFS_IOCTL_SETRENAME;
        }
	else{
		op = -1;
	}
	return op;
}
#endif

int main(int argc,const char *argv[]){
	int rc=0, fd = -1; 
	uint32_t bitmap=0;
	char *mounted_path = NULL, *ptr = NULL;
	#ifndef EC
		char *cmd = NULL;		
	#else
		int i, op;
		uint32_t old_bitmap=0;
	#endif

	if(argc < 2){
		printf("Too few arguments\n");
		goto out;
	}
	mounted_path = (char *)argv[argc-1];
	if((fd = open(mounted_path, O_RDONLY))<0){
        	rc = -1;
                goto out;
        }
	if(argc == 2){
                rc = ioctl(fd, TRFS_IOCTL_GETBITMAP, &bitmap);
                if(rc){
                        goto out;
                }
                printf("Current value of bitmap(in hex): %04x\n", bitmap);
		goto out;
        }

	#ifndef EC

	if(argc == 3){
		cmd = (char *)argv[1];
		if(!strcmp(cmd,"all")){
			//enable tracing for all
			bitmap = TRFS_IOC_ALL;
		}
		else if(!strcmp(cmd,"none")){
			//enable tracing for none
			bitmap = 0;
		}
		else if(!strncmp(cmd,"0x",2)){
			bitmap =  strtoul(cmd, &ptr, 16);
		}
		else{
			printf("Invalid CMD option\n");
			goto out;
		}

                rc = ioctl(fd, TRFS_IOCTL_SETBITMAP, &bitmap);
                if(rc){
                        goto out;
                }
	}
	else{
		printf("Invalid number of arguments\n");
		goto out;
	}	
	#else
		i = 1;
		rc = ioctl(fd, TRFS_IOCTL_GETBITMAP, &old_bitmap);
                if(rc){
                        goto out;
                }
		while(i < (argc-1)){
			switch(argv[i][0]){
			case '-':
				bitmap = 0;
				break;
			case '+':
				bitmap = 1;
				break;
			case 'a':
				bitmap = TRFS_IOC_ALL;
				break;
			case 'n':
				bitmap = 0;
				break;
			case '0':
				bitmap = strtoul((char *)argv[i], &ptr, 16);
				break;
			}
			op = get_trfs_op((char *)++argv[i]);
			if(op == -1){
				printf("Invalid operation\n");
				rc = ioctl(fd, TRFS_IOCTL_SETBITMAP, &old_bitmap);
				goto out;
			}
			rc = ioctl(fd, op, &bitmap);
	                if(rc){
        	                goto out;
                	}
			i++;
		}
	#endif
	
out:
	if(mounted_path){
		close(fd);
	}
	if(rc<0){
		perror("Error");
	}
	exit(rc);
}
