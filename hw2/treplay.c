#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include "../fs/trfs/trfsdefs.h"
#include "userlist.c"

void print_record(struct trfs_record *rec){
	printf("----------------------\n");
	printf("id: %d\n",rec->id);
        printf("size: %d\n",rec->size_of_record);
        printf("type: %c\n",rec->type);
        printf("err/#bytes: %d\n",rec->err);
        printf("pathname: %s\n",rec->pathname);

}

int get_fd(long fileptr, char *path, unsigned int mode, unsigned int flags){
	int fd = -1;
	fd = find(fileptr);
	return fd;
}

char* get_filepath(char *pathname){
	/*set path of directory to replay trace file*/
	char * basepath = "../tempcpy";
	char *filepath;
        filepath = malloc(strlen(basepath)+strlen(pathname));
      	strcpy(filepath, basepath);
        strcat(filepath, pathname);

	return filepath;
}

int main(int argc, char *argv[])
{
	short len=0;
	int rc=0, flag=0, res;
	int fd= -1, readf=0;
	int fd_op = -1, readf_op = 0,wr_op = 0;
	char *tfile = NULL;
	char *buf = NULL, *filepath = NULL, *newpath = NULL;
	char *buf_op = NULL;
	struct trfs_record *rec;
	struct read_write *rw_rec;
	int n_flag = 0, s_flag = 0, deviation = 0;

	while((flag = getopt(argc, argv, "nsh")) != -1){
                switch(flag){
			case 'h':
				printf("It is a program to read the trace file and replay its operations.\n");
				printf("Usage: ./xmergesort [-ns] TFILE\n");
				printf("TFILE: trace file to replay\n");
				printf("-n: show the records that will be replayed without replaying them\n");
				printf("-s: strict mode\n");
				printf("-h: display help message\n");
				goto out;
			case 'n':
				n_flag = 1;
                                break;
                        case 's':
				s_flag = 1;
                                break;
                        case '?':
                                printf("Invalid option %c \n",flag);
                                break;
                }
        }
	if(n_flag && s_flag){
                printf("Only one flag can be specified.\n");
                goto out;
        }
	tfile = (char *)argv[optind];
	if(!tfile){	
		printf("Error: Too few arguments. Use -h for help.\n");
		goto out;
	}
	if(argv[optind+1] != NULL){
                printf("Error: Too many arguments. Use -h for help.\n");
                goto out;
        }
	if ((fd = open(tfile, O_RDONLY)) < 0) {
	        perror("open");
	        rc = -1;
		goto out;
	}
	buf = (char *)malloc(sizeof(unsigned int));
	readf = read(fd, buf, sizeof(unsigned int));
	while(readf>0){
		memset(buf, 0, sizeof(unsigned int));
		readf = read(fd, buf, sizeof(short));
        	len = *(short *)buf;
		lseek(fd, -(sizeof(unsigned int)+sizeof(short)), SEEK_CUR);
		free(buf);
		buf = (char *)malloc(len);
        	readf = read(fd, buf, len);
	        rec = (struct trfs_record *)buf;
		print_record(rec);
		if(n_flag)
			goto read_next;
		switch(rec->type){
		case 'r':
			rw_rec = (struct read_write *)TRFS_EXTRA(buf, rec);
			printf("count: %zd\n", rw_rec->count);
			printf("data read: %s\n", rw_rec->data_buf);
        		filepath = get_filepath(rec->pathname);
			fd_op = get_fd(rec->fileptr, filepath, rec->mode, rec->flags);
			if(fd_op == -1){
				printf("Deviation observed in replay\n");
				deviation = 1;
				break;
			}
			buf_op = malloc(rw_rec->count);
			memset(buf_op, '\0', rw_rec->count);
			lseek(fd_op, rw_rec->ppos, SEEK_SET);
        		readf_op = read(fd_op, buf_op, rw_rec->count);
        		printf("Replay result:\n");
			printf("#bytes : %d\n", readf_op);
			printf("buf: %s\n", buf_op);
			if(readf_op != rec->err || strcmp(buf_op, rw_rec->data_buf)){
				printf("Deviation observed in replay\n");
				deviation = 1;
			}
			else{
				printf("Successfully replayed\n");
				deviation = 0;
			}
			break;
		case 'w':
			rw_rec = (struct read_write *)TRFS_EXTRA(buf, rec);
			printf("count: %zd\n", rw_rec->count);
			printf("data: %s\n", rw_rec->data_buf);
        		filepath = get_filepath(rec->pathname);
			fd_op = get_fd(rec->fileptr, filepath, rec->mode, rec->flags);
			if(fd_op == -1){
				printf(" cannot find file .Deviation observed in replay\n");
				deviation = 1;
				break;
			}
			//lseek(fd_op,0, SEEK_END);
			
        		wr_op = write(fd_op,rw_rec->data_buf, rw_rec->count);
			printf("Replay write result:\n");
			printf("readf_op %d \n",wr_op);
			printf("rec->err %d \n",rec->err);
			printf("error : %s \n",strerror(errno));
			if(wr_op != rec->err){
				printf("Deviation observed in replay\n");
				deviation = 1;
			}
			else{
				printf("Successfully replayed\n");
				deviation = 0;
			}
			break;
		case 'o':
			filepath = get_filepath(rec->pathname);
			fd_op = open(filepath, rec->flags, rec->mode);
			if(fd_op > 0)
				insertNode(rec->fileptr, fd_op);
			printf("fd: %d\n",fd_op);
			if((fd_op < 0 && rec->err == 0) || (fd_op >0 && rec->err<0)){
                                printf("Deviation observed in replay\n");
                                deviation = 1;
                        }
                        else{
                                printf("Successfully replayed\n");
                                deviation = 0;
                        }
			break;
		case 'c':
			filepath = get_filepath(rec->pathname);
			if(filepath[strlen(filepath)-1] == '/')
                                break;
			fd_op = find(rec->fileptr);
			if(fd_op == -1){
				printf("Deviation observed in replay\n");
			}
			else{
				close(get_fd(rec->fileptr, filepath, rec->mode, rec->flags));
				delete(rec->fileptr);
				printf("Successfully replayed\n");
                                deviation = 0;
                        }
                        break;
		case 'm':
			filepath = get_filepath(rec->pathname);
			res = mkdir(filepath, rec->mode);
			if(res != rec->err){
				printf("Deviation observed in replay\n");
                                deviation = 1;
                        }
                        else{
                                printf("Successfully replayed\n");
                                deviation = 0;
                        }
			break;
		case 'n':
                        filepath = get_filepath(rec->pathname);
                        res = rmdir(filepath);
                        if(res != rec->err){
                        	printf("Deviation observed in replay\n");
                                deviation = 1;
                        }
                        else{
                                printf("Successfully replayed\n");
                                deviation = 0;
                        }
                        break;
		case 'l':
                        filepath = get_filepath(rec->pathname);
			newpath = get_filepath((char *)TRFS_EXTRA(buf, rec));
                        res = link(filepath, newpath);
                        if(res != rec->err){
                        	printf("Deviation observed in replay\n");
                                deviation = 1;
                        }
                        else{
                                printf("Successfully replayed\n");
                                deviation = 0;
                        }
                        break;
		case 'u':
                        filepath = get_filepath(rec->pathname);
                        res = unlink(filepath);
                        if(res != rec->err){
                                printf("Deviation observed in replay\n");
                                deviation = 1;
                        }
                        else{
                                printf("Successfully replayed\n");
                                deviation = 0;
                        }
                        break;
		case 's':
                        newpath = get_filepath(rec->pathname);
                        filepath = get_filepath((char *)TRFS_EXTRA(buf, rec));
                        res = symlink(filepath, newpath);
                        if(res != rec->err){
                                printf("Deviation observed in replay\n");
                                deviation = 1;
                        }
                        else{
                                printf("Successfully replayed\n");
                                deviation = 0;
                        }
                        break;
		case 'e':
                        filepath = get_filepath(rec->pathname);
                        newpath = get_filepath((char *)TRFS_EXTRA(buf, rec));
                        res = rename(filepath, newpath);
                        if(res != rec->err){
                                printf("Deviation observed in replay\n");
                                deviation = 1;
                        }
                        else{
                                printf("Successfully replayed\n");
                                deviation = 0;
                        }
                        break;
		default:
			printf("Invalid type\n");

		}
		if(s_flag && deviation)
			goto out;

		read_next:
		free(buf);
		if(buf_op){
			free(buf_op);
			buf_op = NULL;
		}
		if(filepath){
			free(filepath);
			filepath = NULL;
		}
		if(newpath){
                        free(newpath);
			newpath = NULL;
		}
                rw_rec = NULL;
                buf = (char *)malloc(sizeof(unsigned int));
                readf = read(fd, buf, sizeof(unsigned int));
	}

	out:
	if(tfile){
		close(fd);
	}
	if(buf){
		free(buf);
	}
	if(rc<0){
		perror("Error");
	}
	exit(rc);
}
